//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//           INSTITUTO POLIT�CNICO DO C�VADO E DO AVE
//                          2018/2019
//             ENGENHARIA DE SISTEMAS INFORM�TICOS
//                    VIS�O POR COMPUTADOR
//
//             [  SANDRO QUEIR�S - squeiros@ipca.pt  ]
//             [  DAVIDE AZEVEDO - a14426@alunos.ipca.pt  ]
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <stdio.h>
#include <time.h>
#include "vc.h"
#define _CRT_SECURE_NO_WARNINGS

int main_VC032() {

	int i, nblobs = 0;
	OVC *blobs = NULL;
	IVC *image[2];

	image[0] = vc_read_image("Images/tp/imagens/Imagem11.ppm");
	if (image[0] == NULL)
	{
		printf("ERROR -> vc_read_image():\n\tFile not found!\n");
		getchar();
		return 0;
	}
	if (vc_rgb_to_hsv(image[0]) != 1) {
		printf("ERROR -> vc_rgb_to_hsv():\n");
		getchar();
		return 0;
	}

	/*vc_write_image("Results/vc-0032hsv.ppm", image[0]);
	system("FilterGear Results/vc-0032hsv.ppm");
	*/
	vc_check_all_colors(image[0], nblobs, blobs);

	/*vc_image_free(image[0]);
	vc_image_free(image[1]);*/

	/*printf("Press any key to exit...\n");
	getchar();*/
	return 0;
}

int main_VC033() {

	int i;
	int nblobs = 0;
	OVC *blobs = NULL;
	IVC *image[2];
	int value;

	image[0] = vc_read_image("Images/tp/imagens/Imagem11.ppm");
	if (image[0] == NULL)
	{
		printf("ERROR -> vc_read_image():\n\tFile not found!\n");
		getchar();
		return 0;
	}
	image[1] = vc_image_new(image[0]->width, image[0]->height, image[0]->channels, image[0]->levels);
	for (int i = 0; i < (image[0]->height*image[0]->width*image[0]->channels); i++) image[1]->data[i] = image[0]->data[i];

	if (vc_rgb_to_hsv(image[0]) != 1) {
		printf("ERROR -> vc_rgb_to_hsv():\n");
		getchar();
		return 0;
	}

	value = 1;
	blobs = vc_check_color(image[0], 0, 255, 0, 30, 174, 239, &nblobs, value);//white

	if (blobs != NULL)					//testar todas a cores!!!!!!!
	{
		printf("\nNumber of chips: %d\n", nblobs);
		vc_print_all_info(blobs, nblobs);
		vc_drawn_boundbox(image[1], blobs, nblobs);
		
		//vc_write_image("Results/vc-0032rgbbound.ppm", image[1]);
		//system("FilterGear Results/vc-0032rgbbound.ppm");
	}
	free(blobs);
	blobs = NULL;
	nblobs = 0;
	value = 5;
	blobs = vc_check_color(image[0], 240, 8, 98, 175, 147, 225, &nblobs, value);//red

	if (blobs != NULL)					//testar todas a cores!!!!!!!
	{
		printf("\nNumber of chips: %d\n", nblobs);
		vc_print_all_info(blobs, nblobs);
		vc_drawn_boundbox(image[1], blobs, nblobs);

		//vc_write_image("Results/vc-0032rgbbound.ppm", image[1]);
		//system("FilterGear Results/vc-0032rgbbound.ppm");
	}


	free(blobs);
	blobs = NULL;
	nblobs = 0;
	value = 25;
	blobs = vc_check_color(image[0], 124, 136, 158, 255, 125, 213, &nblobs, value);//cian

	if (blobs != NULL)					//testar todas a cores!!!!!!!
	{
		printf("\nNumber of chips: %d\n", nblobs);
		vc_print_all_info(blobs, nblobs);
		vc_drawn_boundbox(image[1], blobs, nblobs);

		//vc_write_image("Results/vc-0032rgbbound.ppm", image[1]);
		//system("FilterGear Results/vc-0032rgbbound.ppm");
	}


	free(blobs);
	blobs = NULL;
	nblobs = 0;
	value = 50;
	blobs = vc_check_color(image[0], 147, 156, 123, 242, 84, 198, &nblobs, value);//blue

	if (blobs != NULL)					//testar todas a cores!!!!!!!
	{
		printf("\nNumber of chips: %d\n", nblobs);
		vc_print_all_info(blobs, nblobs);
		vc_drawn_boundbox(image[1], blobs, nblobs);

		//vc_write_image("Results/vc-0032rgbbound.ppm", image[1]);
		//system("FilterGear Results/vc-0032rgbbound.ppm");
	}

	free(blobs);
	blobs = NULL;
	nblobs = 0;
	value = 100;
	blobs = vc_check_color(image[0], 130, 180, 25, 146, 20, 109, &nblobs, value);//black

	if (blobs != NULL)					//testar todas a cores!!!!!!!
	{
		printf("\nNumber of chips: %d\n", nblobs);
		vc_print_all_info(blobs, nblobs);
		vc_drawn_boundbox(image[1], blobs, nblobs);

		//vc_write_image("Results/vc-0032rgbbound.ppm", image[1]);
		//system("FilterGear Results/vc-0032rgbbound.ppm");
	}


	/*vc_write_image("Results/vc-0032rgbbound.ppm", image[1]);
	system("FilterGear Results/vc-0032rgbbound.ppm");*/
	//vc_check_all_colors(image[0], nblobs, blobs);

	/*vc_image_free(image[0]);
	vc_image_free(image[1]);*/

	/*printf("Press any key to exit...\n");
	getchar();*/
	return 0;
}


int main(void) {
	clock_t tic = clock();

	main_VC033();
	clock_t toc = clock();
	printf("Elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);
	getchar();
	return 0;
}


