//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//           INSTITUTO POLIT�CNICO DO C�VADO E DO AVE
//                          2018/2019
//             ENGENHARIA DE SISTEMAS INFORM�TICOS
//                    VIS�O POR COMPUTADOR
//
//             [  SANDRO QUEIR�S - squeiros@ipca.pt  ]
//             [  DAVIDE AZEVEDO - a14426@alunos.ipca.pt  ]
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#define VC_DEBUG


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                   ESTRUTURA DE UMA IMAGEM
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


typedef struct {
	unsigned char *data;
	int width, height;
	int channels;			// Bin�rio/Cinzentos=1; RGB=3
	int levels;				// Bin�rio=1; Cinzentos [1,255]; RGB [1,255]
	int bytesperline;		// width * channels
} IVC;

typedef struct {
	int x, y, width, height; //Caixa delimitadora(Boundig Box)
	int area;				//�rea
	int xc, yc;				//Centro Massa
	int perimeter;			//Perimetro
	int label;				//Etiqueta
	int defect; //defeito =1, sem defeito =0
	int value; //valor da ficha
} OVC;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                    PROT�TIPOS DE FUN��ES
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FUN��ES: ALOCAR E LIBERTAR UMA IMAGEM
IVC *vc_image_new(int width, int height, int channels, int levels);
IVC *vc_image_free(IVC *image);

// FUN��ES: LEITURA E ESCRITA DE IMAGENS (PBM, PGM E PPM)
IVC *vc_read_image(char *filename);
int vc_write_image(char *filename, IVC *image);

int vc_rgb_to_hsv(IVC *srcdst);

int vc_hsv_to_binary(IVC *src, IVC *dst, int threesholdH1, int threesholdH2, int threesholdS1, int threesholdS2, int threesholdV1, int threesholdV2);

int vc_binary_dilate(IVC *src, IVC *dst, int size);
int vc_binary_erode(IVC *src, IVC *dst, int size);

int vc_binary_open(IVC *src, IVC *dst, int sizeerode, int sizedilate);
int vc_binary_close(IVC *src, IVC *dst, int sizeerode, int sizedilate);

OVC* vc_binary_blob_labelling(IVC *src, IVC *dst, int *nLabels);
int vc_binary_blob_info(IVC *src, OVC *blobs, int nblobs);

OVC* vc_check_color(IVC *src, int threesholdH1, int threesholdH2, int threesholdS1, int threesholdS2, int threesholdV1, int threesholdV2, int *nblobs, int value);

int vc_check_all_colors(IVC *src, OVC *blobs, int *nblobs);

int vc_check_defect(IVC *src, OVC *blobs, int nblobs);

int vc_drawn_boundbox(IVC* srcdst, OVC *blobs, int nblobs);
int vc_boundbox_maker(IVC* srcdst, OVC blob, unsigned char r, unsigned char g, unsigned char b);

void vc_print_all_info(OVC *blobs, int nblobs);

void vc_print_info(OVC blob);