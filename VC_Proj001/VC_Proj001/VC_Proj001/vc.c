//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//           INSTITUTO POLIT�CNICO DO C�VADO E DO AVE
//                          2018/2019
//             ENGENHARIA DE SISTEMAS INFORM�TICOS
//                    VIS�O POR COMPUTADOR
//
//             [  SANDRO QUEIR�S - squeiros@ipca.pt  ]
//             [  DAVIDE AZEVEDO - a14426@alunos.ipca.pt  ]
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "vc.h"
#include <minmax.h>
#define _CRT_SECURE_NO_WARNINGS

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//            FUN��ES: ALOCAR E LIBERTAR UMA IMAGEM
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// Alocar mem�ria para uma imagem
IVC *vc_image_new(int width, int height, int channels, int levels)
{
	IVC *image = (IVC *) malloc(sizeof(IVC));

	if(image == NULL) return NULL;
	if((levels <= 0) || (levels > 255)) return NULL;

	image->width = width;
	image->height = height;
	image->channels = channels;
	image->levels = levels;
	image->bytesperline = image->width * image->channels;
	image->data = (unsigned char *) malloc(image->width * image->height * image->channels * sizeof(char));

	if(image->data == NULL)
	{
		return vc_image_free(image);
	}

	return image;
}


// Libertar mem�ria de uma imagem
IVC *vc_image_free(IVC *image)
{
	if(image != NULL)
	{
		if(image->data != NULL)
		{
			free(image->data);
			image->data = NULL;
		}

		free(image);
		image = NULL;
	}

	return image;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    FUN��ES: LEITURA E ESCRITA DE IMAGENS (PBM, PGM E PPM)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


char *netpbm_get_token(FILE *file, char *tok, int len)
{
	char *t;
	int c;
	
	for(;;)
	{
		while(isspace(c = getc(file)));
		if(c != '#') break;
		do c = getc(file);
		while((c != '\n') && (c != EOF));
		if(c == EOF) break;
	}
	
	t = tok;
	
	if(c != EOF)
	{
		do
		{
			*t++ = c;
			c = getc(file);
		} while((!isspace(c)) && (c != '#') && (c != EOF) && (t - tok < len - 1));
		
		if(c == '#') ungetc(c, file);
	}
	
	*t = 0;
	
	return tok;
}

long int unsigned_char_to_bit(unsigned char *datauchar, unsigned char *databit, int width, int height)
{
	int x, y;
	int countbits;
	long int pos, counttotalbytes;
	unsigned char *p = databit;

	*p = 0;
	countbits = 1;
	counttotalbytes = 0;

	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = width * y + x;

			if(countbits <= 8)
			{
				// Numa imagem PBM:
				// 1 = Preto
				// 0 = Branco
				//*p |= (datauchar[pos] != 0) << (8 - countbits);
				
				// Na nossa imagem:
				// 1 = Branco
				// 0 = Preto
				*p |= (datauchar[pos] == 0) << (8 - countbits);

				countbits++;
			}
			if((countbits > 8) || (x == width - 1))
			{
				p++;
				*p = 0;
				countbits = 1;
				counttotalbytes++;
			}
		}
	}

	return counttotalbytes;
}

void bit_to_unsigned_char(unsigned char *databit, unsigned char *datauchar, int width, int height)
{
	int x, y;
	int countbits;
	long int pos;
	unsigned char *p = databit;

	countbits = 1;

	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = width * y + x;

			if(countbits <= 8)
			{
				// Numa imagem PBM:
				// 1 = Preto
				// 0 = Branco
				//datauchar[pos] = (*p & (1 << (8 - countbits))) ? 1 : 0;

				// Na nossa imagem:
				// 1 = Branco
				// 0 = Preto
				datauchar[pos] = (*p & (1 << (8 - countbits))) ? 0 : 1;
				
				countbits++;
			}
			if((countbits > 8) || (x == width - 1))
			{
				p++;
				countbits = 1;
			}
		}
	}
}

IVC *vc_read_image(char *filename)
{
	FILE *file = NULL;
	IVC *image = NULL;
	unsigned char *tmp;
	char tok[20];
	long int size, sizeofbinarydata;
	int width, height, channels;
	int levels = 255;
	int v;
	
	// Abre o ficheiro
	if((file = fopen(filename, "rb")) != NULL)
	{
		// Efectua a leitura do header
		netpbm_get_token(file, tok, sizeof(tok));

		if(strcmp(tok, "P4") == 0) { channels = 1; levels = 1; }	// Se PBM (Binary [0,1])
		else if(strcmp(tok, "P5") == 0) channels = 1;				// Se PGM (Gray [0,MAX(level,255)])
		else if(strcmp(tok, "P6") == 0) channels = 3;				// Se PPM (RGB [0,MAX(level,255)])
		else
		{
			#ifdef VC_DEBUG
			printf("ERROR -> vc_read_image():\n\tFile is not a valid PBM, PGM or PPM file.\n\tBad magic number!\n");
			#endif

			fclose(file);
			return NULL;
		}
		
		if(levels == 1) // PBM
		{
			if(sscanf(netpbm_get_token(file, tok, sizeof(tok)), "%d", &width) != 1 || 
			   sscanf(netpbm_get_token(file, tok, sizeof(tok)), "%d", &height) != 1)
			{
				#ifdef VC_DEBUG
				printf("ERROR -> vc_read_image():\n\tFile is not a valid PBM file.\n\tBad size!\n");
				#endif

				fclose(file);
				return NULL;
			}

			// Aloca mem�ria para imagem
			image = vc_image_new(width, height, channels, levels);
			if(image == NULL) return NULL;

			sizeofbinarydata = (image->width / 8 + ((image->width % 8) ? 1 : 0)) * image->height;
			tmp = (unsigned char *) malloc(sizeofbinarydata);
			if(tmp == NULL) return 0;

			#ifdef VC_DEBUG
			printf("\nchannels=%d w=%d h=%d levels=%d\n", image->channels, image->width, image->height, levels);
			#endif

			if((v = fread(tmp, sizeof(unsigned char), sizeofbinarydata, file)) != sizeofbinarydata)
			{
				#ifdef VC_DEBUG
				printf("ERROR -> vc_read_image():\n\tPremature EOF on file.\n");
				#endif

				vc_image_free(image);
				fclose(file);
				free(tmp);
				return NULL;
			}

			bit_to_unsigned_char(tmp, image->data, image->width, image->height);

			free(tmp);
		}
		else // PGM ou PPM
		{
			if(sscanf(netpbm_get_token(file, tok, sizeof(tok)), "%d", &width) != 1 || 
			   sscanf(netpbm_get_token(file, tok, sizeof(tok)), "%d", &height) != 1 || 
			   sscanf(netpbm_get_token(file, tok, sizeof(tok)), "%d", &levels) != 1 || levels <= 0 || levels > 255)
			{
				#ifdef VC_DEBUG
				printf("ERROR -> vc_read_image():\n\tFile is not a valid PGM or PPM file.\n\tBad size!\n");
				#endif

				fclose(file);
				return NULL;
			}

			// Aloca mem�ria para imagem
			image = vc_image_new(width, height, channels, levels);
			if(image == NULL) return NULL;

			#ifdef VC_DEBUG
			printf("\nchannels=%d w=%d h=%d levels=%d\n", image->channels, image->width, image->height, levels);
			#endif

			size = image->width * image->height * image->channels;

			if((v = fread(image->data, sizeof(unsigned char), size, file)) != size)
			{
				#ifdef VC_DEBUG
				printf("ERROR -> vc_read_image():\n\tPremature EOF on file.\n");
				#endif

				vc_image_free(image);
				fclose(file);
				return NULL;
			}
		}
		
		fclose(file);
	}
	else
	{
		#ifdef VC_DEBUG
		printf("ERROR -> vc_read_image():\n\tFile not found.\n");
		#endif
	}
	
	return image;
}

int vc_write_image(char *filename, IVC *image)
{
	FILE *file = NULL;
	unsigned char *tmp;
	long int totalbytes, sizeofbinarydata;
	
	if(image == NULL) return 0;

	if((file = fopen(filename, "wb")) != NULL)
	{
		if(image->levels == 1)
		{
			sizeofbinarydata = (image->width / 8 + ((image->width % 8) ? 1 : 0)) * image->height + 1;
			tmp = (unsigned char *) malloc(sizeofbinarydata);
			if(tmp == NULL) return 0;
			
			fprintf(file, "%s %d %d\n", "P4", image->width, image->height);
			
			totalbytes = unsigned_char_to_bit(image->data, tmp, image->width, image->height);
			printf("Total = %ld\n", totalbytes);
			if(fwrite(tmp, sizeof(unsigned char), totalbytes, file) != totalbytes)
			{
				#ifdef VC_DEBUG
				fprintf(stderr, "ERROR -> vc_read_image():\n\tError writing PBM, PGM or PPM file.\n");
				#endif

				fclose(file);
				free(tmp);
				return 0;
			}

			free(tmp);
		}
		else
		{
			fprintf(file, "%s %d %d 255\n", (image->channels == 1) ? "P5" : "P6", image->width, image->height);
		
			if(fwrite(image->data, image->bytesperline, image->height, file) != image->height)
			{
				#ifdef VC_DEBUG
				fprintf(stderr, "ERROR -> vc_read_image():\n\tError writing PBM, PGM or PPM file.\n");
				#endif

				fclose(file);
				return 0;
			}
		}
		
		fclose(file);

		return 1;
	}
	
	return 0;
}

int vc_rgb_to_hsv(IVC *srcdst) {

	int x, y;
	long int pos;
	float h, s, v;
	float r, g, b;
	float max, min;

	if (srcdst->height <= 0 || srcdst->width <= 0 || srcdst->data == NULL) return 0;
	if (srcdst->channels != 3) return 0;

	for (x = 0; x < srcdst->width; x++)
	{
		for (y = 0; y < srcdst->height; y++)
		{
			pos = y * srcdst->bytesperline + x * srcdst->channels;
			
			r = (float)srcdst->data[pos];
			g = (float)srcdst->data[pos + 1];
			b = (float)srcdst->data[pos + 2];
			
			max = (r > g ? (r > b ? r : b) : (g > b ? g : b));
			min = (r < g ? (r < b ? r : b) : (g < b ? g : b));
			
			v = max;
			s = v == 0 ? 0 : max == min ? 0 : (float)((max-min)/v);

			if (max == min) {
				h = 0;
			}
			else {
				if (max == r && g >= b ) h = 60.0f * (g - b) / (max - min);
				if (max == r && b > g) h = 360.0f + 60.0f * (g - b) / (max - min);
				if (max == g) h = 120.0f + 60.0f * (b - r) / (max - min);
				if (max == b) h = 240.0f + 60.0f * (r - g) / (max - min);
				if (max == min) h = 0;
			}
			srcdst->data[pos] = (unsigned char)(h/360.0f*255);
			srcdst->data[pos + 1] = (unsigned char)(s*255);
			srcdst->data[pos + 2] = (unsigned char)v;
		}
	}
	return 1;
}

int vc_hsv_to_binary(IVC *src, IVC *dst, int threesholdH1, int threesholdH2, int threesholdS1, int threesholdS2, int threesholdV1, int threesholdV2)
{	
	int x, y;
	int pos, posb;
	int thH1, thH2, thH3, thH4;
	int red = 0;

	if (src->height <= 0 || src->width <= 0 || src->data == NULL) return 0;
	if (src->channels != 3 || src->levels != 255) return 0;
	if (dst->height <= 0 || dst->width <= 0 || dst->data == NULL) return 0;
	if (dst->channels != 1 || dst->levels != 1) return 0;
	if (src->width != dst->width || src->height != dst->height) return 0;

	if (threesholdH1 > 212 && threesholdH2 < 43)
	{
		thH1 = 0;
		thH2 = threesholdH2;
		thH3 = threesholdH1;
		thH4 = 255;
		red = 1;
	}

	for (y = 0; y < src->height; y++)
	{
		for (x = 0; x < src->width; x++)
		{
			pos = y * src->bytesperline + x * src->channels;
			posb = y * dst->bytesperline + x;
			//(src->data[pos] > threesholdH1 && src->data[pos] < threesholdH2) || (src->data[pos] >= 0 && src->data[pos] < 43) || (src->data[pos]>212 && src->data[pos]<=255)
			//src->data[pos] > threesholdH1 && src->data[pos] < threesholdH2
			if (red == 1) {
				if (((src->data[pos] < threesholdH2 && src->data[pos] >= 0) || (src->data[pos] >= threesholdH1 && src->data[pos] < 255)) &&
					(src->data[pos + 1] > threesholdS1 && src->data[pos + 1] < threesholdS2) &&
					(src->data[pos + 2] > threesholdV1 && src->data[pos + 2] < threesholdV2)) {
					dst->data[posb] = 1;
				}
				else {
					dst->data[posb] = 0;
				}
			}
			else {
				if ((src->data[pos] > threesholdH1 && src->data[pos] < threesholdH2) &&
					(src->data[pos + 1] > threesholdS1 && src->data[pos + 1] < threesholdS2) &&
					(src->data[pos + 2] > threesholdV1 && src->data[pos + 2] < threesholdV2)) {

					dst->data[posb] = 1;
				}
				else {
					dst->data[posb] = 0;
				}
			}
			/*dst->data[pos] = src->data[pos] > threesholdH1 && src->data[pos] < threesholdH2 ?
						(src->data[pos + 1] > threesholdS1 && src->data[pos + 1] < threesholdH2 ?
							(src->data[pos + 2] > threesholdV1 && src->data[pos + 2] < threesholdV2 ? 1 : 0 ) : 0) :0;*/
		}
	}

	return 1;
}

int vc_binary_dilate(IVC *src, IVC *dst, int size) {

	int x, y,xx,yy;
	int halfkernel = (size - 1) / 2; ;
	long int pos,pos1;
	int find = 0;

	if (src->height <= 0 || src->width <= 0 || src->data == NULL) return 0;
	if (src->channels != 1 || src->levels != 1) return 0;
	if (dst->height <= 0 || dst->width <= 0 || dst->data == NULL) return 0;
	if (dst->channels != 1 || dst->levels != 1) return 0;
	if (src->width != dst->width || src->height != dst->height) return 0;

	for (x = 0; x < src->width * src->height; x++)
		dst->data[x] = src->data[x];

	for (x = 0; x < src->width; x++)
	{
		for (y = 0; y < src->height; y++)
		{
			pos = y * src->bytesperline + x;
			find = 0;
			for (xx = x - halfkernel < 0 ? 0 : x - halfkernel; xx <= (x + halfkernel >= src->width ? src->width - 1 : x + halfkernel); xx++)
			{
				for (yy = y - halfkernel < 0 ? 0 : y - halfkernel; yy <= (y + halfkernel >= src->height ? src->height - 1 : y + halfkernel); yy++)
				{
					pos1 = yy * src->bytesperline + xx;
					if (src->data[pos1] == 1) {
						dst->data[pos] = 1;
						find = 1;
						break;
					}
				}
				if (find == 1) break;
			}
		}
	}
	return 1;
}

int vc_binary_erode(IVC *src, IVC *dst, int size) {

	int x, y, xx, yy;
	int halfkernel = (size - 1) / 2; ;
	long int pos, pos1;
	int find;

	if (src->height <= 0 || src->width <= 0 || src->data == NULL) return 0;
	if (src->channels != 1 || src->levels != 1 ) return 0;
	if (dst->height <= 0 || dst->width <= 0 || dst->data == NULL) return 0;
	if (dst->channels != 1 || dst->levels != 1) return 0;
	if (src->width != dst->width || src->height != dst->height) return 0;

	for (x = 0; x < src->width * src->height; x++)
		dst->data[x] = src->data[x];

	for (x = 0; x < src->width; x++)
	{
		for (y = 0; y < src->height; y++)
		{
			pos = y * src->bytesperline + x;
			find = 0;
			for (xx = x - halfkernel < 0 ? 0 : x - halfkernel; xx <= (x + halfkernel >= src->width ? src->width - 1 : x + halfkernel); xx++)
			{
				for (yy = y - halfkernel < 0 ? 0 : y - halfkernel; yy <= (y + halfkernel >= src->height ? src->height - 1 : y + halfkernel); yy++)
				{
					pos1 = yy * src->bytesperline + xx;
					if (src->data[pos1] == 0) {
						dst->data[pos] = 0;
						find = 1;
						break;
					}
				}
				if (find == 1) break;
			}
		}
	}
	return 1;
}
//erode -> dilate
int vc_binary_open(IVC *src, IVC *dst, int sizeerode, int sizedilate) {

	IVC *image;
	
	if (src->height <= 0 || src->width <= 0 || src->data == NULL) return 0;
	if (src->channels != 1 || src->levels != 1) return 0;	
	if (dst->height <= 0 || dst->width <= 0 || dst->data == NULL) return 0;
	if (dst->channels != 1 || dst->levels != 1) return 0;
	if (src->width != dst->width || src->height != dst->height) return 0;

	image = vc_image_new(src->width, src->height, 1, 1);

	vc_binary_erode(src, image, sizeerode);
	
	vc_binary_dilate(image, dst, sizedilate);
	vc_image_free(image);
	return 1;
}
//dilate -> erode
int vc_binary_close(IVC *src, IVC *dst, int sizeerode, int sizedilate) {

	IVC *image;
	int x;

	if (src->height <= 0 || src->width <= 0 || src->data == NULL) return 0;
	if (src->channels != 1 || src->levels != 1) return 0;
	if (dst->height <= 0 || dst->width <= 0 || dst->data == NULL) return 0;
	if (dst->channels != 1 || dst->levels != 1) return 0;
	if (src->width != dst->width || src->height != dst->height) return 0;

	image = vc_image_new(src->width, src->height, 1, 1);

	vc_binary_dilate(src, image, sizedilate);

	vc_binary_erode(image, dst, sizeerode);
	vc_image_free(image);
	return 1;
}

OVC * vc_binary_blob_labelling(IVC * src, IVC * dst, int *nLabels)
{
	unsigned char *datasrc = (unsigned char *)src->data;
	unsigned char *datadst = (unsigned char *)dst->data;
	int width = src->width;
	int height = src->height;
	int bytesperline = src->bytesperline;
	int channels = src->channels;
	int x, y, a, b;
	long int i, size;
	long int posX, posA, posB, posC, posD;
	int labeltable[256] = { 0 };
	int labelarea[256] = { 0 };
	int label = 1; // Etiqueta inicial.
	int num;
	OVC *blobs; // Apontador para lista de blobs (objectos) que ser? retornada desta fun??o.

	// Verifica??o de erros
	if ((src->width <= 0) || (src->height <= 0) || (src->data == NULL)) return 0;
	if ((src->width != dst->width) || (src->height != dst->height) || (src->channels != dst->channels)) return NULL;
	if (channels != 1) return NULL;

	// Copia dados da imagem bin?ria para imagem grayscale
	memcpy(datadst, datasrc, bytesperline * height);

	// Todos os pix?is de plano de fundo devem obrigat?riamente ter valor 0
	// Todos os pix?is de primeiro plano devem obrigat?riamente ter valor 255
	// Ser?o atribu?das etiquetas no intervalo [1,254]
	// Este algoritmo est? assim limitado a 254 labels
	for (i = 0, size = bytesperline * height; i < size; i++)
	{
		if (datadst[i] != 0) datadst[i] = 255;
	}

	// Limpa os rebordos da imagem bin?ria
	for (y = 0; y < height; y++)
	{
		datadst[y * bytesperline + 0 * channels] = 0;
		datadst[y * bytesperline + (width - 1) * channels] = 0;
	}
	for (x = 0; x < width; x++)
	{
		datadst[0 * bytesperline + x * channels] = 0;
		datadst[(height - 1) * bytesperline + x * channels] = 0;
	}

	// Efectua a etiquetagem
	for (y = 1; y < height - 1; y++)
	{
		for (x = 1; x < width - 1; x++)
		{
			// Kernel:
			// A B C
			// D X

			posA = (y - 1) * bytesperline + (x - 1) * channels; // A
			posB = (y - 1) * bytesperline + x * channels; // B
			posC = (y - 1) * bytesperline + (x + 1) * channels; // C
			posD = y * bytesperline + (x - 1) * channels; // D
			posX = y * bytesperline + x * channels; // X

			// Se o pixel foi marcado
			if (datadst[posX] != 0)
			{
				if ((datadst[posA] == 0) && (datadst[posB] == 0) && (datadst[posC] == 0) && (datadst[posD] == 0))
				{
					datadst[posX] = label;
					labeltable[label] = label;
					label++;
				}
				else
				{
					num = 255;

					// Se A est? marcado, j? tem etiqueta (j? n?o ? 255), e ? menor que a etiqueta "num"
					if ((datadst[posA] != 0) && (datadst[posA] != 255) && (datadst[posA] < num))
					{
						num = datadst[posA];
					}
					// Se B est? marcado, j? tem etiqueta (j? n?o ? 255), e ? menor que a etiqueta "num"
					if ((datadst[posB] != 0) && (datadst[posB] != 255) && (datadst[posB] < num))
					{
						num = datadst[posB];
					}
					// Se C est? marcado, j? tem etiqueta (j? n?o ? 255), e ? menor que a etiqueta "num"
					if ((datadst[posC] != 0) && (datadst[posC] != 255) && (datadst[posC] < num))
					{
						num = datadst[posC];
					}
					// Se D est? marcado, j? tem etiqueta (j? n?o ? 255), e ? menor que a etiqueta "num"
					if ((datadst[posD] != 0) && (datadst[posD] != 255) && (datadst[posD] < num))
					{
						num = datadst[posD];
					}

					// Actualiza a tabela de etiquetas
					if ((datadst[posA] != 0) && (datadst[posA] != 255))
					{
						if (labeltable[datadst[posA]] != labeltable[num])
						{
							for (a = 1; a < label; a++)
							{
								if (labeltable[a] == labeltable[datadst[posA]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					if ((datadst[posB] != 0) && (datadst[posB] != 255))
					{
						if (labeltable[datadst[posB]] != labeltable[num])
						{
							for (a = 1; a < label; a++)
							{
								if (labeltable[a] == labeltable[datadst[posB]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					if ((datadst[posC] != 0) && (datadst[posC] != 255))
					{
						if (labeltable[datadst[posC]] != labeltable[num])
						{
							for (a = 1; a < label; a++)
							{
								if (labeltable[a] == labeltable[datadst[posC]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					if ((datadst[posD] != 0) && (datadst[posD] != 255))
					{
						if (labeltable[datadst[posD]] != labeltable[num])
						{
							for (a = 1; a < label; a++)
							{
								if (labeltable[a] == labeltable[datadst[posD]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					labeltable[datadst[posX]] = num;

					// Atribui a etiqueta ao pixel
					datadst[posX] = num;
				}
			}
		}
	}

	// Volta a etiquetar a imagem
	for (y = 1; y < height - 1; y++)
	{
		for (x = 1; x < width - 1; x++)
		{
			posX = y * bytesperline + x * channels; // X

			if (datadst[posX] != 0)
			{
				datadst[posX] = labeltable[datadst[posX]];
			}
		}
	}

	// Contagem do n?mero de blobs
	// Passo 1: Eliminar, da tabela, etiquetas repetidas
	for (a = 1; a < label - 1; a++)
	{
		for (b = a + 1; b < label; b++)
		{
			if (labeltable[a] == labeltable[b]) labeltable[b] = 0;
		}
	}
	// Passo 2: Conta etiquetas e organiza a tabela de etiquetas, para que n?o hajam valores vazios (zero) entre etiquetas
	*nLabels = 0;
	for (a = 1; a < label; a++)
	{
		if (labeltable[a] != 0)
		{
			labeltable[*nLabels] = labeltable[a]; // Organiza tabela de etiquetas
			(*nLabels)++; // Conta etiquetas
		}
	}

	// Se n?o h? blobs
	if (*nLabels == 0) return NULL;

	// Cria lista de blobs (objectos) e preenche a etiqueta
	blobs = (OVC *)calloc((*nLabels), sizeof(OVC));
	if (blobs != NULL)
	{
		for (a = 0; a < (*nLabels); a++) blobs[a].label = labeltable[a];
	}
	else return NULL;

	return blobs;
}

int vc_binary_blob_info(IVC *src, OVC *blobs, int nblobs) {
	
	int x, y,i,xx,yy, perm;
	long int pos, posA, posB, posC, posD;
	int xmin, xmax, ymin, ymax, area;
	
	for (i = 0; i < nblobs; i++){
		xmin = src->width;
		ymin = src->height;
		xmax = ymax = -1;
		xx = yy = 0;
		blobs[i].area = 0;
		blobs[i].perimeter = 0;
		
		for (y = 1; y < src->height-1; y++)
		{
			for (x = 1; x < src->width-1; x++)
			{
				pos = y * src->bytesperline + x;
				if (src->data[pos] == blobs[i].label) {
					xmin = xmin > x ? x : xmin;
					xmax = xmax < x ? x : xmax;
					ymin = ymin > y ? y : ymin;
					ymax = ymax < y ? y : ymax;
					blobs[i].area++;
					xx += x;
					yy += y;
					//  A
					//B X C
					//  D
					if ((src->data[pos - 1] != blobs[i].label || src->data[pos + 1] != blobs[i].label || src->data[pos - src->bytesperline] != blobs[i].label || src->data[pos + src->bytesperline] != blobs[i].label) && src->data[pos]==blobs[i].label)
						blobs[i].perimeter++;
				}
			}
		}
		blobs[i].x = xmin;
		blobs[i].y = ymin;
		blobs[i].width = xmax - xmin + 1;
		blobs[i].height = ymax - ymin + 1;
		blobs[i].xc = xx / (blobs[i].area);
		blobs[i].yc = yy / (blobs[i].area);
	}

	return 1;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//            FUN��ES: checar e separar as cores de fichas
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// checar e separar as cores de fichas
//imagem src entra em hsv!!!!
int vc_check_all_colors(IVC * src, OVC *blobs, int *nblobs) {

	//image[i], image1[i] binaria
	IVC *image[5], *image1[5];
	//valor da ficha
	int value;
	
	for (int i = 0; i < 5; i++)
	{
		image[i] = vc_image_new(src->width, src->height, 1, 1);
		image1[i] = vc_image_new(src->width, src->height, 1, 1);
		for (int x = 0; x < (src->width * src->height); x++) {
			image[i]->data[x] = 0;
			image1[i]->data[x] = 0;
		}
	}
	//for (int x = 0; x < (src->width * src->height)*3; x++) src1->data[x] = src->data[x];

	vc_hsv_to_binary(src, image[0], 0, 255, 0, 30, 174, 239); //branco working
	vc_hsv_to_binary(src, image[1], 240, 8, 98, 175, 147, 225); //vermelho working
	vc_hsv_to_binary(src, image[2], 124, 136, 158, 255, 125, 213);//azul ciano, working
	vc_hsv_to_binary(src, image[3], 147, 156, 130, 255, 49, 173);//azul, testar melhor
	vc_hsv_to_binary(src, image[4], 130, 180, 25, 180, 9, 109); //preto working

	/*vc_write_image("Results/vc-0032bd.pbm", image[3]);
	system("FilterGear Results/vc-0032bd.pbm");*/
	for (int j = 0; j < 5; j++)
	{
		vc_binary_open(image[j], image1[j], 3, 3);
		vc_binary_close(image1[j], image[j], 13, 9);
		vc_write_image("Results/vc-0032bd.pbm", image[j]);
		system("FilterGear Results/vc-0032bd.pbm");
		blobs = vc_binary_blob_labelling(image[j], image1[j], &nblobs);

		value = j == 0 ? 1 : j == 1 ? 5 : j == 2 ? 25 : j == 3 ? 50 : 100;

		for (int q = 0; q < nblobs; q++)
		{
			blobs[q].value = value;
		}
		/*vc_write_image("Results/vc-0032bin.ppm", image1[j]);
		system("FilterGear Results/vc-0032bin.ppm");*/

		if (blobs != NULL)
		{
			vc_binary_blob_info(image1[j], blobs, nblobs);
			clear_blobs(blobs, &nblobs);
			vc_check_defect(image[j], blobs, nblobs);
			
			printf("\nNumber of labels: %d\n", nblobs);
			for (int i = 0; i < nblobs; i++)
			{
				//printf("-> Label %d\n", blobs[i].label);
				printf("	-> Area %d\n", blobs[i].area);
				printf("	-> Perimeter %d\n", blobs[i].perimeter);
				printf("	-> Defect %d\n", blobs[i].defect);
				printf("	-> Value %d\n", blobs[i].value);
			}
			vc_image_free(image[j]);
			vc_image_free(image1[j]);
			blobs = NULL;
		}
	}
	//vc_hsv_to_binary(image[0], image[1], 132, 157, 24, 114, 44, 88);//preto

	return 1;

}

//elimina blobs considererados, com area < 1200
//basicamente desconsidera 'ruidos'
int clear_blobs(OVC *blobs, int *nblobs) {
	//variavel que guarda o numero de blobs
	int last = *nblobs;
	for (int i = 0; i < (*nblobs); i++)
	{
		if (blobs[i].area < 2000 ) {

			for (int j = i; j < (*nblobs)-1; j++) {
				blobs[j] = blobs[j + 1];
			}
			(*nblobs)--;
			i--;
		}
	}
	return 1;
}

OVC* vc_check_color(IVC * src, int threesholdH1, int threesholdH2, int threesholdS1, int threesholdS2, int threesholdV1, int threesholdV2, int *nblo, int value) {
	//a imagem ja entra hsv!!!!!!

	IVC *image[2];
	OVC* blobs = NULL;
	int nblobs = 0;
	

	for (int i = 0; i < 2; i++)
	{
		image[i] = vc_image_new(src->width, src->height, 1, 1);
		/*for (int x = 0; x < (src->width * src->height); x++) {
			image[i]->data[x] = 0;
		}*/
	}

	vc_hsv_to_binary(src, image[0], threesholdH1, threesholdH2, threesholdS1, threesholdS2, threesholdV1, threesholdV2);

	vc_binary_close(image[0], image[1], 11, 9);
	vc_binary_open(image[1], image[0], 9, 13);
	
	blobs = vc_binary_blob_labelling(image[0], image[1], &nblobs);

	//vc_write_image("Results/vc-0032bin.pbm", image[0]);
	//system("FilterGear Results/vc-0032bin.pbm");

	if (blobs != NULL)
	{	
		vc_binary_blob_info(image[1], blobs, nblobs);
		clear_blobs(blobs, &nblobs);
		vc_check_defect(image[0], blobs, nblobs);

		for (int i = 0; i < nblobs; i++)
		{
			blobs[i].value = value;
		}

		//printf("\nNumber of labels: %d\n", nblobs);
		(*nblo) = nblobs;
		/*for (i = 0; i < nblobs; i++)
		{
			printf("-> Label %d\n", blobs[i].label);
			printf("-> Area %d\n", blobs[i].area);
			printf("-> Perimeter %d\n", blobs[i].perimeter);
			printf("	-> Defect %d\n", blobs[i].defect);
			printf("	-> Value %d\n", blobs[i].value);
		}*/
		
		//free(blobs);
	}
	
	return blobs;
}

int vc_check_defect(IVC *src, OVC *blobs, int nblobs) {
	//IVC **image;
	//image = (IVC **)malloc((nblobs), sizeof(IVC *) + 1);
	int pos, pos1;
	OVC *dblobs;
	IVC *dst, *image;
	int dnblobs;

	for (int i = 0; i < nblobs; i++) {
		image = vc_image_new(blobs[i].width, blobs[i].height, 1, 1);
		dnblobs = 0;
		
		for (int y = 0; y < blobs[i].height; y++) {
			for (int x = 0; x < blobs[i].width; x++) {		
				pos = (y + blobs[i].y) * src->bytesperline + (x + blobs[i].x);
				pos1 = y * image->bytesperline + x;
				image->data[pos1] = src->data[pos] == 1 ? 0 : 1;
			}
		}	
		//vc_write_image("Results/vc-0032bin.pbm", image);
		//system("FilterGear Results/vc-0032bin.pbm");
		
		dst = vc_image_new(image->width, image->height, 1, 255);
		dblobs = vc_binary_blob_labelling(image, dst, &dnblobs);
		//ate aqui OK!!!!

		if (dblobs != NULL)
		{
			vc_binary_blob_info(dst, dblobs, dnblobs);
			for (int ii = 0; ii < dnblobs; ii++)
			{
				if (dblobs[ii].x != 1 && dblobs[ii].y != 1
					&& (dblobs[ii].width + dblobs[ii].x + 1) != blobs[i].width
					&& (dblobs[ii].height + dblobs[ii].y + 1) != blobs[i].height) {

					blobs[i].defect = 1;

					/*printf("	data X%d\n", dblobs[ii].x);
					printf("	data Y%d\n", dblobs[ii].y);
					printf("	data Width%d\n", dblobs[ii].width);
					printf("	data Height%d\n", dblobs[ii].height);*/
				}
			}
			//free(dblobs);
			vc_image_free(dst);
		}
		vc_image_free(image);
	}
	return 1;
}

int vc_drawn_boundbox(IVC* srcdst, OVC *blobs, int nblobs) {
	
	int pos;
	//cm -> centro massa, altura * largura da cruz
	int cm = 15;
	for (int i = 0; i < nblobs; i++)
	{
		if (blobs[i].defect == 0) vc_boundbox_maker(srcdst, blobs[i], 0, 255, 0);
		if (blobs[i].defect == 1) vc_boundbox_maker(srcdst, blobs[i], 255, 0, 0);
	}
}

int vc_boundbox_maker(IVC* srcdst, OVC blob, unsigned char r, unsigned char g, unsigned char b) {

	int pos;
	//cm -> centro massa, altura * largura da cruz
	int cm = 15;

	for (int x = blob.x; x < (blob.x + blob.width); x++)
	{
		pos = blob.y * srcdst->bytesperline + x * srcdst->channels;
		srcdst->data[pos] = r;
		srcdst->data[pos + 1] = g;
		srcdst->data[pos + 2] = b;

		srcdst->data[pos - srcdst->bytesperline] = r;
		srcdst->data[pos + 1 - srcdst->bytesperline] = g;
		srcdst->data[pos + 2 - srcdst->bytesperline] = b;


		pos = (blob.y + blob.height) * srcdst->bytesperline + x * srcdst->channels;
		srcdst->data[pos] = r;
		srcdst->data[pos + 1] = g;
		srcdst->data[pos + 2] = b;

		srcdst->data[pos + srcdst->bytesperline] = r;
		srcdst->data[pos + 1 + srcdst->bytesperline] = g;
		srcdst->data[pos + 2 + srcdst->bytesperline] = b;

	}
	for (int y = blob.y; y < (blob.y + blob.height); y++)
	{
		pos = y * srcdst->bytesperline + blob.x * srcdst->channels;
		srcdst->data[pos] = r;
		srcdst->data[pos + 1] = g;
		srcdst->data[pos + 2] = b;

		srcdst->data[pos - srcdst->channels] = r;
		srcdst->data[pos + 1 - srcdst->channels] = g;
		srcdst->data[pos + 2 - srcdst->channels] = b;

		pos = y * srcdst->bytesperline + (blob.x + blob.width) * srcdst->channels;
		srcdst->data[pos - srcdst->bytesperline] = r;
		srcdst->data[pos + 1 - srcdst->bytesperline] = g;
		srcdst->data[pos + 2 - srcdst->bytesperline] = b;

		srcdst->data[pos + srcdst->channels] = r;
		srcdst->data[pos + 1 + srcdst->channels] = g;
		srcdst->data[pos + 2 + srcdst->channels] = b;
	}

	for (int x = blob.xc - cm; x < blob.xc + cm; x++)
	{
		pos = blob.yc * srcdst->bytesperline + x * srcdst->channels;
		srcdst->data[pos] = r;
		srcdst->data[pos + 1] = g;
		srcdst->data[pos + 2] = b;

		srcdst->data[pos - srcdst->bytesperline] = r;
		srcdst->data[pos + 1 - srcdst->bytesperline] = g;
		srcdst->data[pos + 2 - srcdst->bytesperline] = b;
	}
	for (int y = blob.yc - cm; y < blob.yc + cm; y++)
	{
		pos = y * srcdst->bytesperline + blob.xc * srcdst->channels;
		srcdst->data[pos] = r;
		srcdst->data[pos + 1] = g;
		srcdst->data[pos + 2] = b;

		srcdst->data[pos + srcdst->channels] = r;
		srcdst->data[pos + 1 + srcdst->channels] = g;
		srcdst->data[pos + 2 + srcdst->channels] = b;
	}

	return 1;
}

void vc_print_all_info(OVC *blobs, int nblobs) {

	int cvalue = 0;

	for (int i = 0; i < nblobs; i++)
	{
		if (blobs[i].value ==1)
		{
			printf("White Chip.\n");
			vc_print_info(blobs[i]);
			cvalue += blobs[i].value;
		}
		if (blobs[i].value == 5)
		{
			printf("Red Chip.\n");
			vc_print_info(blobs[i]);
			cvalue += blobs[i].value;
		}
		if (blobs[i].value == 25)
		{
			printf("Cyan Chip.\n");
			vc_print_info(blobs[i]);
			cvalue += blobs[i].value;
		}
		if (blobs[i].value == 50)
		{
			printf("Blue Chip.\n");
			vc_print_info(blobs[i]);
			cvalue += blobs[i].value;
		}
		if (blobs[i].value == 100)
		{
			printf("Black Chip.\n");
			vc_print_info(blobs[i]);
			cvalue += blobs[i].value;
		}
		if (blobs[i].value == 0)
		{
			printf("Blind.\n");
			vc_print_info(blobs[i]);
		}
	}
}

void vc_print_info(OVC blob) {

	printf("  -> Area %d\n", blob.area);
	printf("  -> Perimeter %d\n", blob.perimeter);
	printf("  -> Defect %s\n", blob.defect == 1 ? "YES" : "NO");
}